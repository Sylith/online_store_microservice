from fastapi import APIRouter, Body, Query
from fastapi.encoders import jsonable_encoder
from typing import Optional, Dict

from server.database import (
    add_item,
    retrieve_items_names,
    retrieve_item,
)
from server.models.item import (
    ErrorResponseModel,
    ResponseModel,
    ItemSchema,
)

router = APIRouter()

@router.post("/", response_description="Items retrieved")
async def get_items(title: Optional[str] = Query(None), params: Optional[Dict[str, str]] = Body(None)):
    items = await retrieve_items_names(title, params)
    if items:
        return ResponseModel(items, "Items names retrieved successfully")
    return ResponseModel(items, "Empty list returned")

@router.post("/new", response_description="Item data added into the database")
async def add_item_data(item: ItemSchema = Body(...)):
    item = jsonable_encoder(item)
    new_item = await add_item(item)
    return ResponseModel(new_item, "Item added successfully.")

@router.get("/{id}", response_description="Item data added into the database")
async def get_item(id: str):
    item = await retrieve_item(id)
    if item:
        return ResponseModel(item, "Item data retrieved successfully")
    return ResponseModel(item, "No such item")