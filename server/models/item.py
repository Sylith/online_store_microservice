from typing import Optional, Dict
from pydantic import BaseModel, Field

class ItemSchema(BaseModel):
    title: str = Field(...)
    description: str = Field(...)
    params: Dict[str, str] = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "title": "IPhone",
                "description": "brand new iphone",
                "params": {
                    "camera": "true",
                    "memory": "256 gb"
                }
            }
        }


def ResponseModel(data, message):
    return {
        "data": data,
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}