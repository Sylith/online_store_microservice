import motor.motor_asyncio
from bson.objectid import ObjectId
from typing import Optional, Dict

client = motor.motor_asyncio.AsyncIOMotorClient('localhost', 27017)
db = client.items_database
collection = db.items_collection


def item_helper(item) -> dict:
    return {
        "id": str(item["_id"]),
        "title": item["title"],
        "description": item["description"],
        "params": item["params"]
    }


def short_item_helper(item) -> dict:
    return {
        "id": str(item["_id"]),
        "title": item["title"]
    }


async def retrieve_items_names(title: Optional[str] = None, params: Optional[Dict[str, str]] = None):
    items = []
    async for item in collection.find():
        if title and params:
            if (title.lower() in item["title"].lower()) and (set(params.items()).issubset(set(item["params"].items()))):
                items.append(short_item_helper(item))
        elif title:
            if (title.lower() in item["title"].lower()):
                items.append(short_item_helper(item))
        elif params:
            if (set(params.items()).issubset(set(item["params"].items()))):
                items.append(short_item_helper(item))
        else:
            items.append(short_item_helper(item))
    return items


async def add_item(item_data: dict) -> dict:
    item = await collection.insert_one(item_data)
    new_item = await collection.find_one({"_id": item.inserted_id})
    return item_helper(new_item)


async def retrieve_item(id: str) -> dict:
    item = await collection.find_one({"_id": ObjectId(id)})
    return item_helper(item)
