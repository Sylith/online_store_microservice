# Микросервис для магазина

## Шаги для инсталяции

1. `pip install -r requirements.txt`
2. `docker run -d -p 27017:27017 mongo`

## Комнада для запуска

`python main.py`

## Curl команды

### Создание товара

`curl -X POST "http://127.0.0.1:8000/item/new" -H "accept: application/json" -H "Content-Type: application/json" -d "{\"title\":\"IPhone\",\"description\":\"brand new iphone\",\"params\":{\"camera\":\"true\",\"memory\":\"256 gb\"}}"`

### Получение названий всех товаров

`curl -X POST "http://127.0.0.1:8000/item/" -H "accept: application/json" -H "Content-Type: application/json" -d ""`

### Получение названий товаров с фильтрацией по названию и параметрам

`curl -X POST "http://127.0.0.1:8000/item/?title=Iphone" -H "accept: application/json" -H "Content-Type: application/json" -d "{\"camera\":\"true\",\"memory\":\"256 gb\"}"`

### Получение всех деталей товара по id

`curl -X GET "http://127.0.0.1:8000/item/5fbcfdb70458d6e53caba58f" -H "accept: application/json"`

## Краткое описание

Микросервис выполнен с помощью фреймворка FastApi, в качестве базы данных используется запущенная в docker контейнере MongoDB с асинхронным драйвером motor.
